package hello;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
public class HelloWorldController {
    private final GreetingService greetingService;

    public HelloWorldController(GreetingService greetingService) {
        this.greetingService = greetingService;
    }

    @GetMapping("/hello-world")
    @ResponseBody
    public List<Greeting> sayHello(
            @RequestParam(name = "name", required = false,
            defaultValue = "Stranger") List<String> names) {

        return greetingService.sayHello(names);
    }

    @GetMapping("/greetings")
    @ResponseBody
    public List<Greeting> greetings() {
        return greetingService.findGreeting();
    }

    @PostMapping("/greetings")
    @ResponseBody
    public Greeting addGreeting(@RequestBody AddNamesDTO dto) {
        return greetingService.addGreeting(List.of(dto.name));
    }
}
