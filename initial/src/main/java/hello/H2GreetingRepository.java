package hello;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class H2GreetingRepository implements GreetingRepository {
    private final JdbcTemplate jdbcTemplate;

    public H2GreetingRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void save(Greeting greeting) {
        jdbcTemplate.update("INSERT INTO greeting(id, name) values(?, ?)",
                greeting.getId(), greeting.getName());
    }

    @Override
    public List<Greeting> find() {
        return jdbcTemplate.query("SELECT id, name FROM greeting",
                (rs, rowNum) -> new Greeting(rs.getString("id"),
                        rs.getString("name")));
    }
}
