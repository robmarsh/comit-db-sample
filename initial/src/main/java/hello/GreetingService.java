package hello;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Component
public class GreetingService {
    private final GreetingRepository greetingRepository;

    public GreetingService(GreetingRepository greetingRepository) {
        this.greetingRepository = greetingRepository;
    }

    public List<Greeting> sayHello(List<String> names) {
        Greeting greeting = new Greeting(
                UUID.randomUUID().toString(),
                String.join(", ", names)
        );

        greetingRepository.save(greeting);

        return greetingRepository.find();
    }

    public Greeting addGreeting(List<String> names) {
        Greeting greeting = new Greeting(
                UUID.randomUUID().toString(),
                String.join(", ", names)
        );

        greetingRepository.save(greeting);

        return greeting;
    }

    public List<Greeting> findGreeting() {
        return greetingRepository.find();
    }
}
